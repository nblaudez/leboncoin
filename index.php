<?php

define('ROOT', dirname(__DIR__ . '/..'));
require ROOT . '/app/App.php';
App::load();
parse_str($_SERVER['REDIRECT_QUERY_STRING'],$_GET);
$p = $_GET['p'] ?? $_POST['p'];
$ws = false;
if ($p != null) {
    $page = $p;
    if($p == "email" || $p == "palindrome") {
        $ws = true;
    }
} else {
    $page = 'contact.index';
}



if($ws === true) {
    require ROOT . '/app/Components/Api/Api.php';
} else {
    $page = explode('.', $page);
    $controller = '\App\Controllers\\' . ucfirst($page[0]) . 'Controller';
    $action = $page[1];
    $controller = new $controller();
    $controller->$action();
}
