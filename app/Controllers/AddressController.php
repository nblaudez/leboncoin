<?php

namespace App\Controllers;

use App;
use App\Controllers\ControllerInterface;

class AddressController extends MainController implements ControllerInterface
{
    /**
     * AddressController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->loadModel('Addresse');
        $this->loadModel('Contact');
    }

    /**
     * Affichage de la liste des adresses d'un Utilisateur
     */
    public function index()
    {
        $idContact = intval($_GET['id']);
        $_SESSION['idContact'] =  intval($_GET['id']);
        $contact = $this->Contact->findById($idContact);
        $address = $this->Addresse->getByContact($idContact);
        echo $this->twig->render('addresslist.html.twig', [
            'addresses' => $address,
            'idContact' => $idContact,
            'contact'   => $contact,
            'flashMessage' => App::getFlashMessage()
        ]);
    }


    /**
     * Ajout d'adresse pour un contact
     */
    public function add()
    {
        $error = false;
        $id = intval($_GET['id']);

        if (!empty($_POST)) {
            // Nettoyage
            $response = $this->sanitize($_POST);

            if ($response["response"]) {

                $idContact = $response['idContact'];
                $result = $this->Addresse->create([
                    'number'     => $response['number'],
                    'city'       => $response['city'],
                    'country'    => $response['country'],
                    'postalCode' => $response['postalCode'],
                    'street'     => $response['street'],
                    'idContact'  => $response['idContact']
                ]);

                if ($result) {
                    $_SESSION['flashMessage'] = "L'adresse a bien été ajoutée";
                    header("Location: /address.index?id=$id");
                } else {
                    $error = true;
                    $this->twig->render('addressadd.html.twig',
                        ["idContact" => $id,'error' => $error]);
                }
            } else {
                $error = true;
                $this->twig->render('addressadd.html.twig',
                    ["idContact" => $id,'error' => $error]);

            }
        }
        echo $this->twig->render('addressadd.html.twig',
            ["idContact" => $id,'error' => $error]);
    }

    /**
     * Modification d'une adresse d'un contact
     */
    public function edit()
    {
        $error = false;
        $id = intval($_GET['id'] ?? $_POST['id']);

        if (!empty($_POST)) {
            $response = $this->sanitize($_POST);


            if ($response["response"]) {
                $addresse = $this->Addresse->findById($id);
                $result = $this->Addresse->update($id,
                    [
                        'number'     => $response['number'],
                        'city'       => $response['city'],
                        'country'    => $response['country'],
                        'postalCode' => $response['postalCode'],
                        'street'     => $response['street'],
                    ]);
                if ($result) {
                    $_SESSION['flashMessage'] = "L'adresse a bien été modifiée";
                    header("Location: /address.index?id=$addresse->idContact");
                } else {
                    $error = true;
                    $this->twig->render('addressedit.html.twig',
                        ["idContact" => $id,'error' => $error]);

                }
            } else {

                $error = true;
                $this->twig->render('addressedit.html.twig',
                    ["idContact" => $id,'error' => $error]);

            }
        }

        $data = $this->Addresse->findById($id);
        echo $this->twig->render('addressedit.html.twig',
            [
                'data'      => $data,
                "idContact" => $data->idContact
            ]);
    }

    /**
     * Suppression d'une adresse d'un contact
     */
    public function delete()
    {
        $result = $this->Addresse->delete($_GET['id']);
        if ($result) {
            $_SESSION['flashMessage'] = "L'adresse à bien été supprimée";
            header('Location: /address.index?id='.$_SESSION['idContact']);
        }
    }


    /**
     * Vérifie les contrainte d'enregistrement
     *
     * @param array $data
     *
     * @return array
     */
    public function sanitize($data = []) : ?array
    {

        $number     = $data['number'];
        $city       = strtoupper($data['city']);
        $country    = strtoupper($data['country']);
        $street     = strtoupper($data['street']);
        $idContact  = intval($data['idContact']);
        $postalCode  = intval($data['postalCode']);

        if ($number && $city && $country && $postalCode && $street
            && is_int($idContact)
        ) {
            return [
                'response'   => true,
                'number'     => $data['number'],
                'city'       => strtoupper($data['city']),
                'country'    => strtoupper($data['country']),
                'postalCode' => $data['postalCode'],
                'street'     => strtoupper($data['street']),
                'idContact'  => $data['idContact']
            ];
        } else {
            return ['response' => false];
        }
    }
}